"""
    Program for list which only except int or float or both
"""


class MyList(list):
    """
        This custom list will only except particular data type set by acceptonly method
    """

    data_types = []

    @classmethod
    def acceptonly(cls, data_type):
        """
            data_type: String or iterable input
            This method will set data type(s) which can be stored in list
        """
        if isinstance(data_type, tuple):
            cls.data_types = list(data_type)
        elif isinstance(data_type, str):
            cls.data_types = [data_type]

    def validate_datatype(self, p_obj):
        for d in self.data_types:
            if isinstance(p_obj, eval(d)):
                return True
        raise ValueError('Not supported data type %s' % (p_obj,))

    def append(self, p_obj):
        self.validate_datatype(p_obj)
        super(MyList, self).append(p_obj)

    def extend(self, iterable):
        for i in iterable:
            self.validate_datatype(i)
        super(MyList, self).extend(iterable)

    def insert(self, index, p_obj):
        self.validate_datatype(p_obj)
        super(MyList, self).insert(index, p_obj)


if __name__ == '__main__':
    MyList.acceptonly(('int', 'float',))
    new_list = MyList()
    new_list.append(5)
    #new_list.append(5.0)
    new_list.extend([10, 20])
    new_list.insert(0, 25)

    print new_list


