import sys
import csv


class Meals(object):
    """
        Class for Meals in town.
    """

    def search_restaurant(self, file_path, *args):
        """
            file_path: This is path of CSV file containing restaurant data
            args: Positional argument for meals to be search
        """
        search_results = []
        final_result = []
        with open(file_path) as f:
            reader = csv.reader(f)
            for row in reader:
                row = map(lambda x: x.strip(), row)
                meals = row[2:]
                if set(meals).intersection(set(args)):
                    search_results.append(row)
        search_results = sorted(search_results, key=lambda x:float(x[0]))
        i = 0
        while i < len(search_results):
            one_res_records = filter(lambda x: x[0] == search_results[i][0], search_results)
            for s in one_res_records:
                value_meal = set(args).intersection(set(s[2:]))
                contains_all = filter(lambda x: x[0] == s[0] and value_meal and len(value_meal) >= len(args), search_results)
                for k in contains_all:
                    final_result.append([int(k[0]), float(k[1])])
                contains_one = filter(lambda x: x[0] == s[0] and value_meal and len(value_meal) < len(args), search_results)
                if contains_one:
                    total = 0
                    for r in contains_one:
                        total += float(r[1])
                    if total:
                        final_result.append([int(contains_one[0][0]), total])

            i += len(one_res_records)
        sorted_total = sorted(search_results, key=lambda x:x[1])
        if sorted_total:
            return sorted_total[0][0], sorted_total[0][1]
        else:
            return None


if __name__ == '__main__':
    if len(sys.argv) >= 3:
        file_path = sys.argv[1]
        input_meals = sys.argv[2:]
        d = Meals()
        print d.search_restaurant(file_path, *input_meals)
    else:
        print 'File Path or Meal is missing.File path and atleast one meal is necessary for searching.'